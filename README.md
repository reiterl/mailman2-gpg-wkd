<!--
SPDX-FileCopyrightText: 2020 Intevation GmbH

SPDX-License-Identifier: MIT
-->

# README #

mailman 2.1 is a well used maillist software. I want to add support for
encrypted and signed mails with python-gpg/wkd to it.

This is tested with mailman 2.1.32

### How am I? ###

My name is Ludwig Reiter. I'm  software developer at Intevation. We use free
software. At the moment I work to add gpg wkd support to mailman 2.1.32.

### What is the purpose of this repository?

Publish in an early stage the code I'm working on. This should be a proof of 
concept. The code is experimental.

### What is required? ###

You need gpgme 1.12.0 and its python extension python-gpg for this.
In debian buster:

- python-gpg

### What do you neet to do to get this running? ###

If you want to encrypt service mails with wkd, do this:

1. Install python-gpg
2. Copy Handlers/Encrypt.py to Mailman/Handlers/
3. Add to Queues/VirginRunner.py 'Encrypt' to the pipeline, before 'InOutgoing'

If you want to sign/encrypt service with wkd, do this:

1. Install python-gpg
2. Copy Handlers/SignEncrypt.py to Mailman Handlers/
3. Add to Queues/VirginRunner.py 'SignEncrypt' to the pipeline,
   before 'InOutgoing'
4. Create a seperate gnupg home directory.
  * E.G. /tmp/dot.gnupg
  * chmod og-rwx /tmp/dot.gnupg
  * chown mailman:mailman /tmp/dot.gnupg
5. Create a gnupg sign key without password and import it into the dot.gnupg
6. Set the two configurations in mm\_cfg.py
  GPG\_GNUPGHOME = '/tmp/dot.gnupg'
  GPG\_SIGN\_KEY = UID of the imported key as string.

### How does this work? ###

UserNotifications and OwnerNotifications are enqueued to the VirginQueue and 
handled by the VirginRunner. So I add a SignEncrypt handler to the pipeline of
this runner.

The handler checks if the configuration is in place, the it tries to locate the
public key with wkd and tries to sign and encrypt.
