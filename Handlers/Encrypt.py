# SPDX-FileCopyrightText: 2020 Intevation GmbH
#
# SPDX-License-Identifier: MIT
#
# Author: Ludwig Reiter <ludwig.reiter@intevation.de>
#
"""Handler which encrypt msgs"""

import os
import shutil
import tempfile
try:
    import gpg
except:
    gpg = None

from email.mime.base import MIMEBase
from Mailman.Logging.Syslog import syslog

# Map gpgme hash algorithm IDs to OpenPGP/MIME micalg strings. GPG
# supports more algorithms than are listed here, but this should cover
# the algorithms that are likely to be used.
hash_algorithms = {
    gpg.constants.MD_SHA1: "pgp-sha1",
    gpg.constants.MD_SHA256: "pgp-sha256",
    gpg.constants.MD_SHA384: "pgp-sha384",
    gpg.constants.MD_SHA512: "pgp-sha512",
    }


def get_key_from_wkd_for(ctx, email):
    ctx.set_ctx_flag("auto-key-locate", "clear,nodefault,wkd")
    keys = ctx.keylist(email, mode=gpg.constants.keylist.mode.LOCATE)
    for key in keys:
        return key


def encrypt_me(gpg_ctx, plain, recipients):
    try:
        gpg_ctx.armor = True
        ciphertext, _, _ = gpg_ctx.encrypt(plain,
                                           recipients=recipients,
                                           sign=False,
                                           always_trust=True)
    except Exception:
        print("OpenPGP encryption failed!")
        raise
    return ciphertext


def create_gnupg_home():
    return tempfile.mkdtemp(prefix='tmp.gpghome')


def process(mlist, msg, msgdata):
    if not gpg:
        syslog('error', 'gpg not found')
        return
    recips = msg.recips
    if len(recips) != 1:
        syslog('error', 'need just one recipient')
        return
    gpghome = create_gnupg_home()
    try:
        os.environ['GNUPGHOME'] = gpghome
        ctx = gpg.Context()
        recip_key = get_key_from_wkd_for(ctx, recips[0])
        if not recip_key:
            syslog('error', 'don\'t find the key')
            return
        ctx.signers = []
        innermsg1 = MIMEBase("application", "pgp-encrypted")
        innermsg1.set_payload("Version: 1")
        body_e = encrypt_me(ctx, msg.as_string(), [recip_key])
        innermsg2 = MIMEBase("application", "octet-stream")
        innermsg2.set_payload(body_e)
        innermsg2.add_header("Content-Disposition",
                             'inline; filename="msg.asc"')
        msg.set_payload([innermsg1, innermsg2])
        msg.replace_header("Content-Type", "multipart/encrypted")
        msg.set_param("protocol", "application/pgp-encrypted")
    finally:
        del os.environ['GNUPGHOME']
        shutil.rmtree(gpghome, ignore_errors=True)
