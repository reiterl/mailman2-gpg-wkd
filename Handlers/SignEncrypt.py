# SPDX-FileCopyrightText: 2020 Intevation GmbH
#
# SPDX-License-Identifier: MIT
#
# Author: Ludwig Reiter <ludwig.reiter@intevation.de>
#
"""Handler which sign and encrypt messages"""

import os
try:
    import gpg
except:
    gpg = None

import email.encoders as encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from Mailman.Logging.Syslog import syslog
from Mailman.mm_cfg import GPG_GNUPGHOME, GPG_SIGN_KEY


# Map gpgme hash algorithm IDs to OpenPGP/MIME micalg strings. GPG
# supports more algorithms than are listed here, but this should cover
# the algorithms that are likely to be used.
hash_algorithms = {
    gpg.constants.MD_SHA1: "pgp-sha1",
    gpg.constants.MD_SHA256: "pgp-sha256",
    gpg.constants.MD_SHA384: "pgp-sha384",
    gpg.constants.MD_SHA512: "pgp-sha512",
    }


def get_key_from_wkd_for(ctx, email):
    """Use the gpg_ctx to get the key from wkd (web keys directory)."""
    ctx.set_ctx_flag("auto-key-locate", "clear,nodefault,wkd")
    keys = ctx.keylist(email, mode=gpg.constants.keylist.mode.LOCATE)
    for key in keys:
        return key


def encrypt_me(gpg_ctx, plain, recipients):
    """Encrypt the plain string with the ctx and the recipients keys.
    Return the encrypted string."""
    try:
        gpg_ctx.armor = True
        ciphertext, _, _ = gpg_ctx.encrypt(plain,
                                           recipients=recipients,
                                           sign=False,
                                           always_trust=True)
    except Exception:
        print("OpenPGP encryption failed!")
        raise
    return ciphertext


def create_signed_part(body, gpg_ctx):
    """Use the gpg_ctx to build a signed mail part from the body."""
    # create inner1 body
    inner1 = MIMEBase("text", "plain")
    inner1.set_payload(body)
    inner1.set_charset("utf-8")
    encoders.encode_quopri(inner1)

    # create inner2 signature
    hash_algo, signature = detached_signature(
        gpg_ctx, inner1.as_string())
    inner2 = MIMEBase("application", "pgp-signature")
    inner2.set_payload(signature)
    encoders.encode_7or8bit(inner2)

    # create outer main message
    outer = MIMEMultipart()
    outer.replace_header("Content-Type", "multipart/signed")
    outer.set_param("protocol", "application/pgp-signature")
    micalg = hash_algorithms.get(hash_algo)
    if micalg is None:
        raise RuntimeError("Unexpected hash algorithm %r from gpgme"
                           % (signature[0].hash_algo,))
    outer.set_param("micalg", micalg)
    outer.attach(inner1)
    outer.attach(inner2)

    return newline_replace(outer.as_string())


def detached_signature(gpg_ctx, plainbytes):
    """Create a detached signature for multipart/signed messages.
    The signature created by this function is asci armored because
    that's required for multipart/signed messages.

    Args:
        gpg_ctx (gpgme context): The gpg context to use for signing.
            The signature is made with whatever keys are set as signing keys
            in this context.
        plainbytes (bytes): The data to sign

    Return:
        Tuple of (hash_algo, signature). The hash_algo is one of the
            relevant constants in gpgme. The signature is a bytestring
            with the signature.
    """

    try:
        gpg_ctx.armor = True
        signed, result = gpg_ctx.sign(newline_replace(plainbytes),
                                      mode=gpg.constants.sig.mode.DETACH)
        sigs = result.signatures
    except Exception:
        print("OpenPGP signing for multipart/signed failed!")
        raise

    return (sigs[0].hash_algo, signed)



def newline_replace(str_):
    """We need <LF><CR> as newlines."""
    return str_.replace("\n", "\r\n")


def process(mlist, msg, msgdata):
    """Main process"""
    if not GPG_GNUPGHOME:
        return

    # gpg should be in place, if gnupg home is configured.
    if not gpg:
        syslog('error', 'gpg not found')
        return

    # check if at least gpg version 1.12.0
    major = int(gpg.version.major)
    minor = int(gpg.version.minor)
    if major == 0 or (major == 1 and minor < 12):
        syslog('error', 'need at least gpg 1.12.0')
        return

    recips = msg.recips
    if len(recips) != 1:
        syslog('error', 'need just one recipient')
        return
    try:
        os.environ['GNUPGHOME'] = GPG_GNUPGHOME
        ctx = gpg.Context()

        recip_key = get_key_from_wkd_for(ctx, recips[0])
        if not recip_key:
            syslog('error', 'don\'t find the key')
            return

        # sign the message if possible
        if GPG_SIGN_KEY:
            ctx.signers = [ctx.get_key(GPG_SIGN_KEY)]
            body = create_signed_part(msg.as_string()+"\n", ctx)
        else:
            body = msg.as_string()

        # encrypt the message
        ctx.signers = []
        innermsg1 = MIMEBase("application", "pgp-encrypted")
        innermsg1.set_payload("Version: 1")
        body_e = encrypt_me(ctx, body, [recip_key])
        innermsg2 = MIMEBase("application", "octet-stream")
        innermsg2.set_payload(body_e)
        innermsg2.add_header("Content-Disposition",
                             'inline; filename="msg.asc"')
        msg.set_payload([innermsg1, innermsg2])
        msg.replace_header("Content-Type", "multipart/encrypted")
        msg.set_param("protocol", "application/pgp-encrypted")
    finally:
        del os.environ['GNUPGHOME']
